using System.Data.Entity;
using AbsencesLedger.DAL.Database.Models;

namespace AbsencesLedger.DAL.Database
{
    public class AbsenceLedgerContext : DbContext
    {
        static AbsenceLedgerContext()
        {
            System.Data.Entity.Database.SetInitializer(new AbsenceLedgerContextInitializer());
        }
        
        public AbsenceLedgerContext() : base("name=DefaultContext")
        {
            
        }

        public DbSet<Absence> Absences { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Position> Positions { get; set; }
        
    }
}