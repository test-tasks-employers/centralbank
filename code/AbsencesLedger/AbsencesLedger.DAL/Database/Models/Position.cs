using System.ComponentModel.DataAnnotations;

namespace AbsencesLedger.DAL.Database.Models
{
    public class Position : Entity
    {
        [Required]
        public string Name { get; set; }
    }
}