using System.ComponentModel.DataAnnotations;

namespace AbsencesLedger.DAL.Database.Models
{
    public abstract class Entity 
    {
        [Key]
        public long Id { get; set; }
    }
}