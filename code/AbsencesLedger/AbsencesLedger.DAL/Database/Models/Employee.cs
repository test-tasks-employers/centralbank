using System.ComponentModel.DataAnnotations;

namespace AbsencesLedger.DAL.Database.Models
{
    public class Employee : Entity
    {
        [Required]
        public string LastName { get; set; }
        
        [Required]
        public string FirstName { get; set; }
        
        public string MiddleName { get; set; }
    }
}