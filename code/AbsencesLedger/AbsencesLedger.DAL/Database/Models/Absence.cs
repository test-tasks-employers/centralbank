using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbsencesLedger.DAL.Database.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Absence : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public Employee Employee { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public long EmployeeId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public long PositionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public Position Position { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Column(TypeName = "DateTime2")]
        public DateTime Date { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public long Duration { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Reason { get; set; }
    }
}