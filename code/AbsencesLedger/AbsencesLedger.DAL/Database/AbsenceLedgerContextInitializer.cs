using System;
using System.Data.Entity;
using AbsencesLedger.DAL.Database.Models;

namespace AbsencesLedger.DAL.Database
{
    public class AbsenceLedgerContextInitializer : DropCreateDatabaseAlways<AbsenceLedgerContext>
    {
        protected override void Seed(AbsenceLedgerContext context)
        {
            var postion1 = new Position() {Name = "Директор"};
            var postion2 = new Position() {Name = "Сотрудник"};
            
            var man1 = new Employee(){FirstName = "Петр", LastName = "Петров"};
            var man2 = new Employee(){FirstName = "Иван", LastName = "Иванов"};
            
            var absence = new Absence(){Employee = man1, Position = postion2, Date = DateTime.Now, Duration = 180, Reason = "По семейным обстоятельствам"};

            context.Employees.Add(man1);
            context.Employees.Add(man2);

            context.Positions.Add(postion1);
            context.Positions.Add(postion2);

            context.Absences.Add(absence);

            context.SaveChanges();
        }
    }
}