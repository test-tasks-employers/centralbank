﻿
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace AbsencesLedger.DAL.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<AbsencesLedger.DAL.Database.AbsenceLedgerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
    } 
}