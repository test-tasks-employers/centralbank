﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AbsencesLedger.DAL.Database.Models;
using AbsencesLedger.Models;
using AbsencesLedger.RequestHandlers;
using MediatR;

namespace AbsencesLedger.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMediator _mediator;

        public HomeController(IMediator mediator)
        {
            _mediator = mediator;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Get(AbsenceListQuery query)
        {
            return Json(await _mediator.Send(query ?? new AbsenceListQuery()),
                JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Post(CreateAbsenceCommand query)
        {
            return Json(await _mediator.Send(query), JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public async Task<ActionResult> Put(UpdateAbsenceCommand query)
        {
            return Json(await _mediator.Send(query), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteAbsenceCommand query)
        {
            return Json(await _mediator.Send(query), JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetEmployees()
        {
            return Json(await _mediator.Send(new EmployeesListQuery()), JsonRequestBehavior.AllowGet);
        }
        
        public async Task<ActionResult> GetPositions()
        {
            return Json(await _mediator.Send(new PositionsListQuery()), JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
    }
}