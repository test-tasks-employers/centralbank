using System;
using System.Collections.Generic;
using AbsencesLedger.RequestHandlers;
using MediatR;

namespace AbsencesLedger.Models
{
    public class AbsenceListQuery : IRequest<AbsenceListResult>
    {
        public string EmployeeContains { get; set; }
        public string PositionContains { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public long? DurationContains { get; set; }
        public string ReasonContains { get; set; }
    }

    public class AbsenceListResult
    {
        public List<AbsenceDto> Absences { get; set; }
    }
}