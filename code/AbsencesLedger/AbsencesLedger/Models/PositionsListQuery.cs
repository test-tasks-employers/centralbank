using System.Collections.Generic;
using MediatR;

namespace AbsencesLedger.Models
{
    public class PositionsListQuery: IRequest<List<PositionDto>>
    {
        
    }

    public class PositionDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}