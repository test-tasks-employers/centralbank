using System.Collections.Generic;
using AbsencesLedger.DAL.Database.Models;
using MediatR;

namespace AbsencesLedger.Models
{
    public class EmployeesListQuery: IRequest<List<EmployeeDto>>
    {
        
    }

    public class EmployeeDto
    {
        public long Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
    }
}