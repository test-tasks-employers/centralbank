﻿var meterPageApp = (function () {
    var appMetrListVm = {
        employees: [],
        newMeter: {
            serialNumber: ""
        }
    }
    var appMetrList = new Vue({
        el: '#meterListArea',
        data: appMetrListVm,
        methods: {
            add: function () {
                var context = this;
                var meter = {
                    SerialNumber: appMetrListVm.newMeter.serialNumber
                }
                axios.post('/meter/add', meter)
                    .then(function (response) {
                        appMetrListVm.newMeter.serialNumber = '';
                        $("#exampleModal").modal('hide');
                        context.get();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            get: function() {
                axios.post('/meter/get')
                    .then(function (response) {
                        appMetrListVm.meters = response.data.Meters;
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        },
        mounted: function() {
            this.get();
        }
    });
    return {
        init: function () {
            
        }
    };
}());
