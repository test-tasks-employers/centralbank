﻿var apartmentPageApp = (function () {
    var appAbsenceLedgerVm = {
        absences: [],
        employees: [],
        positions: [],
        newAbsence: {
            Id : 0,
            EmployeeId: "",
            PositionId: "",
            Date: "",
            Duration: 0,
            Reason: ""
        },
        updateApartmentMeter: {
            Id: 0,
            Value: 0,
            NewId: null
        },
        filter: {
            EmployeeContains: "",
            PositionContains: "",
            Start:"",
            End:"",
            DurationContains:"",
            ReasonContains:""
        }
    };
    var appApartmentList = new Vue({
        el: '#apartmentListArea',
        data: appAbsenceLedgerVm,
        methods: {
            add: function () {
                var context = this;
                axios.post('/home/post', appAbsenceLedgerVm.newAbsence)
                    .then(function (response) {
                        $("#addAbsenceModal").modal('hide');
                        context.get();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            get: function () {
                axios.get('/home/get', {params: appAbsenceLedgerVm.filter})
                    .then(function (response) {
                        appAbsenceLedgerVm.absences = response.data.Absences;
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            getEmployees: function () {
                axios.get('/home/getEmployees')
                    .then(function (response) {
                        appAbsenceLedgerVm.employees = response.data;
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            getPositions: function () {
                axios.get('/home/getPositions')
                    .then(function (response) {
                        appAbsenceLedgerVm.positions = response.data;
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            showUpdateAbsence: function(absence) {
                appAbsenceLedgerVm.newAbsence= absence;
                $("#addAbsenceModal").modal('show');
                $("#updateAbsence").show();
                $("#addNewAbsence").hide();
            },
            update: function() {
                var context = this;
                axios.post('/home/put', appAbsenceLedgerVm.newAbsence)
                    .then(function (response) {
                        $("#addAbsenceModal").modal('hide');
                        context.get();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            del: function(id) {
                var context = this;
                axios.post('/home/delete', { Id: id })
                    .then(function (response) {
                        context.get();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
        },
        mounted: function () {
            var self = this;
            this.show = true;
            this.get();
            $('#addAbsenceModal').on('shown.bs.modal',
                function () {
                    self.getEmployees();
                    self.getPositions();
                });
            
            $('#showAddAbsence').on('click', function () {
                appAbsenceLedgerVm.newAbsence = {
                    Id : 0,
                    EmployeeId: "",
                    PositionId: "",
                    Date: "",
                    Duration: 0,
                    Reason: ""
                };
                $("#updateAbsence").hide();
                $("#addNewAbsence").show();
            });
            $("#datepickerStart").datepicker({
                format: 'dd.mm.yyyy'}).on("changeDate", function (e) {
                self.filter.Start = e.date;});
            
            $("#datepickerEnd").datepicker({
                format: 'dd.mm.yyyy'}).on("changeDate", function (e) {
                self.filter.End = e.date;});

            $("#absenceDate").datepicker({
                format: 'dd.mm.yyyy'}).on("changeDate", function (e) {
                self.newAbsence.Date = e.date;});

        }
    });
    return {
        init: function () {
            
        }
    };
})();