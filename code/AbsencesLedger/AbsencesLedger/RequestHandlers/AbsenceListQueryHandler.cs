using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AbsencesLedger.DAL.Database;
using AbsencesLedger.DAL.Database.Models;
using AbsencesLedger.Models;
using LinqKit;
using MediatR;

namespace AbsencesLedger.RequestHandlers
{
    public class AbsenceListQueryHandler : IRequestHandler<AbsenceListQuery, AbsenceListResult>,
        IRequestHandler<EmployeesListQuery, List<EmployeeDto>>, IRequestHandler<PositionsListQuery, List<PositionDto>>
    {
        private readonly AbsenceLedgerContext _context;

        public AbsenceListQueryHandler(AbsenceLedgerContext context)
        {
            _context = context;
        }

        public async Task<AbsenceListResult> Handle(AbsenceListQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() => new AbsenceListResult()
            {
                Absences = _context.Absences.AsExpandable()
                    .Where(BuildWhereCondition(request))
                    .Select(_ => new AbsenceDto()
                    {
                        Id = _.Id, FirstName = _.Employee.FirstName, LastName = _.Employee.LastName,
                        EmployeeId = _.EmployeeId, PositionId = _.PositionId, Date = _.Date, Duration = _.Duration,
                        MiddleName = _.Employee.MiddleName, Position = _.Position.Name, Reason = _.Reason
                    }).ToList()
            }, cancellationToken);
        }

        private static Expression<Func<Absence, bool>> BuildWhereCondition(AbsenceListQuery query)
        {
            var filters =
                new List<Expression<Func<Absence, bool>>>()
                {
                    EmployeeWhereCondition(query.EmployeeContains),
                    PositionWhereCondition(query.PositionContains),
                    ReasonWhereCondition(query.ReasonContains),
                    DurationWhereCondition(query.DurationContains),
                    DateWhereCondition(query.Start, query.End),
                };

            return filters.Where(_ => _ != null)
                .Aggregate<Expression<Func<Absence, bool>>, Expression<Func<Absence, bool>>>(
                    _ => true, ExpressionHelper.And);
        }

        private static Expression<Func<Absence, bool>> EmployeeWhereCondition(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return null;
            }

            var upperText = text.ToUpper();

            return a => a.Employee.FirstName.ToUpper().Contains(upperText) ||a.Employee.LastName.ToUpper().Contains(upperText) ||
                       a.Employee.MiddleName.ToUpper().Contains(upperText);
        }
        
        private static Expression<Func<Absence, bool>> PositionWhereCondition(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return null;
            }

            var upperText = text.ToUpper();

            return a => a.Position.Name.ToUpper().Contains(upperText);
        }
        
        private static Expression<Func<Absence, bool>> ReasonWhereCondition(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return null;
            }

            var upperText = text.ToUpper();

            return a => a.Reason.ToUpper().Contains(upperText);
        }
        
        private static Expression<Func<Absence, bool>> DurationWhereCondition(long? duration)
        {
            if (duration == null)
            {
                return null;
            }

            return a => a.Duration.ToString().ToUpper().StartsWith(duration.ToString().ToUpper());
        }

        private static Expression<Func<Absence, bool>> DateWhereCondition(DateTime? start, DateTime? end)
        {
            return a => (start == null || a.Date >= start) && (end == null || a.Date <= end);
        }

        public async Task<List<EmployeeDto>> Handle(EmployeesListQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(
                () => _context.Employees.Select(_ => new EmployeeDto()
                    {Id = _.Id, FirstName = _.FirstName, LastName = _.LastName, MiddleName = _.MiddleName}).ToList(),
                cancellationToken);
        }

        public async Task<List<PositionDto>> Handle(PositionsListQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(
                () => _context.Positions.Select(_ => new PositionDto() {Id = _.Id, Name = _.Name}).ToList(),
                cancellationToken);
        }
    }

    public class AbsenceDto
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public long PositionId { get; set; }
        public string Position { get; set; }
        public DateTime Date { get; set; }
        public long Duration { get; set; }
        public string Reason { get; set; }
    }
}
