using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AbsencesLedger.DAL.Database;
using AbsencesLedger.DAL.Database.Models;
using MediatR;

namespace AbsencesLedger.RequestHandlers
{

    public abstract class CreateUpdateAbsenceCommandBase: IRequest<AbsenceCommandResult>
    {
        [Required]
        public long EmployeeId { get; set; }

        [Required]
        public long PositionId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        [Range( 0, 24*60*60, ErrorMessage = "Недопустимое значение продолжительности")]
        public long Duration { get; set; }

        [Required]
        public string Reason { get; set; }
    }
    
    public class CreateAbsenceCommand : CreateUpdateAbsenceCommandBase
    {
        
    }
    

    public class AbsenceCommandHandler : IRequestHandler<CreateAbsenceCommand, AbsenceCommandResult>,
        IRequestHandler<UpdateAbsenceCommand, AbsenceCommandResult>,
        IRequestHandler<DeleteAbsenceCommand, AbsenceCommandResult>
    {
        private readonly AbsenceLedgerContext _context;

        public AbsenceCommandHandler(AbsenceLedgerContext context)
        {
            _context = context;
        }
        public async Task<AbsenceCommandResult> Handle(CreateAbsenceCommand request,
            CancellationToken cancellationToken)
        {
                if (_context.Absences.Any(_ =>
                    _.EmployeeId == request.EmployeeId && _.PositionId == request.PositionId && _.Date == request.Date))
                {
                    throw new BusinessException("Запись для этого сотрудника уже есть");
                }
                
                var employee = await FindEmployeeOrThrowAsync(request.EmployeeId, cancellationToken);
                var position = await FindPositionOrThrowAsync(request.PositionId, cancellationToken);

                var absence = new Absence()
                {
                    Employee = employee,
                    Position = position,
                    Date = request.Date,
                    Duration = request.Duration,
                    Reason = request.Reason
                };

                _context.Absences.Add(absence);
                await _context.SaveChangesAsync(cancellationToken);
                return new AbsenceCommandResult();
        }

        private async Task<Employee> FindEmployeeOrThrowAsync(long id, CancellationToken cancellationToken)
        {
            var employee = await _context.Employees.FirstOrDefaultAsync(_ => _.Id == id, cancellationToken);
            if (employee == null)
            {
                throw new BusinessException("Указанный сотрудник не найден");
            }

            return employee;
        }
        
        private async Task<Position> FindPositionOrThrowAsync(long id, CancellationToken cancellationToken)
        {
            var position = await _context.Positions.FirstOrDefaultAsync(_ => _.Id == id, cancellationToken);
            if (position == null)
            {
                throw new BusinessException("Указанная должность не найдена");
            }

            return position;
        }
        
        private async Task<Absence> FindAbsenceOrThrowAsync(long id, CancellationToken cancellationToken)
        {
            var absence = await _context.Absences.FirstOrDefaultAsync(_ => _.Id == id, cancellationToken);
            if (absence == null)
            {
                throw new BusinessException("Указанная запись не найдена");
            }

            return absence;
        }

        public async Task<AbsenceCommandResult> Handle(UpdateAbsenceCommand request, CancellationToken cancellationToken)
        {

            var absence = await FindAbsenceOrThrowAsync(request.Id, cancellationToken);
            var employee = await FindEmployeeOrThrowAsync(request.EmployeeId, cancellationToken);
            var position = await FindPositionOrThrowAsync(request.PositionId, cancellationToken);

            absence.Date = request.Date;
            absence.Duration = request.Duration;
            absence.Employee = employee;
            absence.Position = position;
            absence.Reason = request.Reason;

            await _context.SaveChangesAsync(cancellationToken);
            
            return new AbsenceCommandResult();
        }

        public async Task<AbsenceCommandResult> Handle(DeleteAbsenceCommand request, CancellationToken cancellationToken)
        {
            var absence = await FindAbsenceOrThrowAsync(request.Id, cancellationToken);

            _context.Absences.Remove(absence);

            await _context.SaveChangesAsync(cancellationToken);
            
            return new AbsenceCommandResult();
        }
    }

    public class DeleteAbsenceCommand : IRequest<AbsenceCommandResult>
    {
        public long Id { get; set; }
    }

    public class AbsenceCommandResult
    {
    }

    public class UpdateAbsenceCommand : CreateUpdateAbsenceCommandBase
    {
        public long Id { get; set; }
    }

    public class BusinessException : Exception
    {
        public BusinessException(string message) :base(message)
        {
        }
    }
}