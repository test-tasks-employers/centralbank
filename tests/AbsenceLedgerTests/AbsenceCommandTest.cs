﻿using System;
using System.Threading;
using AbsencesLedger.DAL.Database;
using AbsencesLedger.DAL.Database.Models;
using AbsencesLedger.RequestHandlers;
using NUnit.Framework;

namespace AbsenceLedgerTests
{
    [TestFixture]
    public class AbsenceCommandTest
    {
        private Employee _employee;

        [SetUp]
        public void Setup()
        {
            using (var context = new AbsenceLedgerContext())
            {
                _employee = new Employee() {FirstName = "FirstName", LastName = "LastName", MiddleName = "MiddleName"};
                context.Employees.Add(_employee);
                context.SaveChanges();
            }
        }
        
        [TearDown]
        public void Teardown()
        {
            using (var context = new AbsenceLedgerContext())
            {
                /*context.Employees.Remove(_employee);
                context.SaveChanges();*/
            }
        }
        
        [Test]
        public void CreateCommandTest()
        {
            var handler = new AbsenceCommandHandler();

            var request = new CreateAbsenceCommand()
            {
                
            };

            Assert.Catch<BusinessException>(() =>
                handler.Handle(request, CancellationToken.None).GetAwaiter().GetResult());

            request.EmployeeId = _employee.Id;
            
            Assert.Catch<BusinessException>(() =>
                handler.Handle(request, CancellationToken.None).GetAwaiter().GetResult());
        }
    }
}