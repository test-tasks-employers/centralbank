﻿using System;
using System.Data.Entity;
using System.Linq;
using AbsencesLedger.DAL.Database;
using AbsencesLedger.DAL.Database.Models;
using NUnit.Framework;

namespace AbsenceLedger.PersistenceTests
{
    [TestFixture]
    public class PositionTest : PersistentTestBase<Position>
    {


        protected override Position CreateObject()
        {
            return CreatePosition();
        }

        protected override DbSet<Position> GetDbSet(AbsenceLedgerContext context)
        {
            return context.Positions;
        }

        protected override Position GetFailedObject()
        {
            return new Position();
        }

        protected override void CheckUpdatedFields(Position obj)
        {
            Assert.AreEqual("NewName", obj.Name);
        }

        protected override void UpdateObject(Position obj)
        {
            obj.Name = "NewName";
        }

        protected override void CheckFields(Position entity)
        {
            Assert.AreEqual("TestPosition", entity.Name);
        }

        [Test]
        public override void CreateUpdateObjectTest()
        {
            base.CreateUpdateObjectTest();
        }
        
        [Test]
        public override void DeleteObjectTest()
        {
            base.DeleteObjectTest();
        }

        [Test]
        public override void ValidationFailedTest()
        {
            base.ValidationFailedTest();
        }

        public static Position CreatePosition()
        {
            return new Position() {Name = "TestPosition"};
        }
    }
}