using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using AbsencesLedger.DAL.Database;
using AbsencesLedger.DAL.Database.Models;
using NUnit.Framework;

namespace AbsenceLedger.PersistenceTests
{
    public abstract class PersistentTestBase<T> where T : Entity
    {
        public virtual void CreateUpdateObjectTest()
        {
            using (var context = new AbsenceLedgerContext())
            {
                var obj = CreateObject();
                var dbSet = GetDbSet(context);
                dbSet.Add(obj);
                context.SaveChanges();

                CheckFields(obj);
                Assert.AreNotEqual(0, obj.Id);
                
                UpdateObject(obj);

                context.SaveChanges();

                CheckUpdatedFields(obj);
            }
        }

        protected  abstract DbSet<T> GetDbSet(AbsenceLedgerContext context);
        
        public virtual void DeleteObjectTest()
        {
            using (var context = new AbsenceLedgerContext())
            {
                var obj = CreateObject();
                var dbSet = GetDbSet(context);
                dbSet.Add(obj);
                context.SaveChanges();

                CheckFields(obj);
                dbSet.Remove(obj);
                context.SaveChanges();
                Assert.IsTrue(dbSet.All(_=>_.Id!=obj.Id));
            }
        }

        public virtual void ValidationFailedTest()
        {
            using (var context = new AbsenceLedgerContext())
            {
                var obj = GetFailedObject();
                var dbSet = GetDbSet(context);
                dbSet.Add(obj);
                var exp =Assert.Catch<DbEntityValidationException>( ()=>context.SaveChanges());
                Console.WriteLine(exp);
            }
        }

        protected abstract T GetFailedObject();

        protected abstract void CheckUpdatedFields(T obj);

        protected abstract void UpdateObject(T entity);
        
        protected abstract void CheckFields(T entity);

        protected abstract T CreateObject();
    }
}