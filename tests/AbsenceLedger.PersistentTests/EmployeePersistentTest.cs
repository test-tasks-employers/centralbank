using System.Data.Entity;
using AbsencesLedger.DAL.Database;
using AbsencesLedger.DAL.Database.Models;
using NUnit.Framework;

namespace AbsenceLedger.PersistenceTests
{
    public class EmployeePersistentTest : PersistentTestBase<Employee>
    {
        private const string UpdatedFirstName = "UpdatedFirstName";
        private const string UpdatedLastName = "UpdatedLastName";
        private const string UpdatedMiddleName = "MiddleName";
        
        private const string FirstName = "FirstName";
        private const string LastName = "LastName";
        private const string MiddleName = "MiddleName";
        
        protected override DbSet<Employee> GetDbSet(AbsenceLedgerContext context)
        {
            return context.Employees;
        }

        protected override Employee GetFailedObject()
        {
            return new Employee();
        }

        protected override void CheckUpdatedFields(Employee obj)
        {
            Assert.AreEqual(UpdatedFirstName, obj.FirstName);
            Assert.AreEqual(UpdatedLastName, obj.LastName);
            Assert.AreEqual(UpdatedMiddleName, obj.MiddleName);
            
        }

        protected override void UpdateObject(Employee entity)
        {
            entity.FirstName = UpdatedFirstName;
            entity.LastName = UpdatedLastName;
            entity.MiddleName = UpdatedMiddleName;
        }

        protected override void CheckFields(Employee entity)
        {
            Assert.AreEqual(FirstName, entity.FirstName);
            Assert.AreEqual(LastName, entity.LastName);
            Assert.AreEqual(MiddleName, entity.MiddleName);
        }

        protected override Employee CreateObject()
        {
            return CreateEmployee();
        }

        [Test]
        public override void CreateUpdateObjectTest()
        {
            base.CreateUpdateObjectTest();
        }

        [Test]
        public override void DeleteObjectTest()
        {
            base.DeleteObjectTest();
        }

        public static Employee CreateEmployee()
        {
            return new Employee() {FirstName = FirstName, LastName = LastName, MiddleName = MiddleName};
        }
    }
}