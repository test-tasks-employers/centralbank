using System;
using System.Data.Entity;
using AbsencesLedger.DAL.Database;
using AbsencesLedger.DAL.Database.Models;
using NUnit.Framework;

namespace AbsenceLedger.PersistenceTests
{
    [TestFixture]
    public class AbsencePersistentTest : PersistentTestBase<Absence>
    {
        private static readonly DateTime Date = DateTime.Now;
        private static readonly DateTime UpdatedDate = Date.AddMonths(-1);
        private static readonly long Duration = 180;
        private static readonly long UpdatedDuration = 280;
        private static readonly string UpdatedReason = "UpdatedReason";
        private const string Reason = "Reason";

        protected override DbSet<Absence> GetDbSet(AbsenceLedgerContext context)
        {
            return context.Absences;
        }

        protected override Absence GetFailedObject()
        {
            return new Absence();
        }

        protected override void CheckUpdatedFields(Absence obj)
        {
            Assert.AreEqual( UpdatedDate, obj.Date);
            Assert.AreEqual( UpdatedDuration, obj.Duration);
            Assert.AreEqual( obj.EmployeeId, obj.Employee.Id);
            Assert.AreEqual( obj.PositionId, obj.Position.Id);
            Assert.AreEqual( UpdatedReason, obj.Reason);
        }

        protected override void UpdateObject(Absence entity)
        {
            entity.Date = UpdatedDate;
            entity.Duration = UpdatedDuration;
            entity.Reason = UpdatedReason;
        }

        protected override void CheckFields(Absence entity)
        {
            Assert.AreEqual( Date, entity.Date);
            Assert.AreEqual( Duration, entity.Duration);
            Assert.AreEqual( entity.EmployeeId, entity.Employee.Id);
            Assert.AreEqual( entity.PositionId, entity.Position.Id);
            Assert.AreEqual( Reason, entity.Reason);
        }

        protected override Absence CreateObject()
        {
            return new Absence()
            {
                Position = PositionTest.CreatePosition(), Employee = EmployeePersistentTest.CreateEmployee(),
                Date = Date, Duration = Duration, Reason = Reason
            };
        }

        [Test]
        public override void CreateUpdateObjectTest()
        {
            base.CreateUpdateObjectTest();
        }

        [Test]
        public override void DeleteObjectTest()
        {
            base.DeleteObjectTest();
        }

        [Test]
        public override void ValidationFailedTest()
        {
            base.ValidationFailedTest();
        }
    }
}